import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import SvgLateralIcon from './Icons/LateralIcon';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/styles';
import Time from './Time'

const styles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
}));

class NavBar extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      timestamp: null
    }
  }

  render() {
    const { classes } = this.props
    return (
      <div className={classes.root}>
        <AppBar position="static">
          <Toolbar>
            
            <Typography variant="h6" className={classes.title} style={{ flex: 1 }} >
              News - <Time timestamp={this.state.timestamp}></Time>
            </Typography>
            <SvgLateralIcon width="34px" height="32px" />
          </Toolbar>
        </AppBar>
      </div>
    );
  }
}

NavBar.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(NavBar);
