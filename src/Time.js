import React, { Component } from 'react';
import API from './api'
import Moment from 'react-moment';
import 'moment-timezone';

class Time extends Component {
  constructor(props){
    super(props);
    const { timestamp } = props
    this.state = { timestamp: timestamp };
  }
  render(){
    return(
      <Moment unix format="DD/MM/YYYY HH:mm:ss">{this.state.timestamp}</Moment>
    );
  }

  getCurrentTime() {
    API.currentTime()
      .then(response => {
        this.setState({timestamp: parseInt(response.data.message)})
        this.interval = setInterval(() => this.setState({ timestamp: this.state.timestamp + 1 }), 1000);
      })
      .catch(response => {
        console.log(response)
      })
  }

  componentDidMount() {
    this.getCurrentTime({})
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }
}

export default Time;
