import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

class RecommendationItem extends React.Component {

  constructor(props) {
    super(props)
    this.classes = makeStyles(theme => ({
      card: {
        maxWidth: 345,
        margin: theme.spacing(1)
      },
      media: {
        height: 140,
      },
    }));
  }

  visitURL(url) {
    window.open(url, "_blank")
  }

  render() {
    const { title, image, summary, url } = this.props

    return (
      <Card className={this.classes.card} onClick={(event) => { this.visitURL(url) }}>
        <CardActionArea>
          <CardMedia
            className={this.classes.media}
            image={image}
            title={title}
          />
          <CardContent>
            <Typography gutterBottom variant="h5" component="h2">
              {title}
            </Typography>
            <Typography variant="body2" color="textSecondary" component="p">
              {summary}
            </Typography>
          </CardContent>
        </CardActionArea>
        <CardActions>
          <Button size="small" color="primary" onClick={(event) => { this.visitURL(url) }}>
            Learn More
          </Button>
        </CardActions>
      </Card>
    );
  }
}

export default (RecommendationItem);
