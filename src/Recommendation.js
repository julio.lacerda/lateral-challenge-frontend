import React from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import Button from '@material-ui/core/Button';
import Tooltip from '@material-ui/core/Tooltip';
import SendIcon from '@material-ui/icons/Send';
import ClearIcon from '@material-ui/icons/Clear';
import SearchIcon from '@material-ui/icons/Search';
import CircularProgress from '@material-ui/core/CircularProgress'
import { green } from '@material-ui/core/colors';
import RecommendationResults from './RecommendationResults'
import Typography from '@material-ui/core/Typography';
import API from './api'
import Container from '@material-ui/core/Container';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/styles';

const styles = makeStyles(theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  wrapper: {
    margin: theme.spacing(1),
    position: 'relative',
  },
  margin: {
    margin: theme.spacing(1),
  },
  button: {
    margin: theme.spacing(1),
  },
  leftIcon: {
    marginRight: theme.spacing(1),
  },
  rightIcon: {
    marginLeft: theme.spacing(1),
  },
  progress: {
    margin: theme.spacing(2),
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: 200,
  },
  dense: {
    marginTop: 19,
  },
  menu: {
    width: 200,
  },
  buttonSuccess: {
    backgroundColor: green[500],
    '&:hover': {
      backgroundColor: green[700],
    },
  },
  buttonProgress: {
    color: green[500],
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -12,
    marginLeft: -12,
  },
}));

class Recommendation extends React.Component {
  
  constructor(props) {
    super(props)
    
    this.recommendAction = this.recommendAction.bind(this);
    this.clearAction = this.clearAction.bind(this);

    this.state = {
      form: {
        fields: {
          text: {
            value: '',
            metadata: {
              id: 'text',
              name: 'text',
              className: props.classes.textField,
              label: 'Text',
              placeholder: 'Enter your text',
              multiline: true,
              margin: 'normal',
              rows: 2,
              rowsMax: "15"
            }
          }
        }
      },
      results: [],
      loading: false,
      success: false,
    }

    this.buttonClassname = clsx({
      [props.classes.buttonSuccess]: this.state.success,
    });

  }

  onChangeFormField(e) {
    const { form } = { ...this.state }
    const currentState = form
    const {name, value} = e.target
    currentState.fields[name].value = value
    this.setState({ form: currentState })
  }

  clearAction(e){
    const { form } = { ...this.state }
    const currentState = form
    currentState.fields.text.value = ''
    this.setState({ form: currentState })
    this.setState({ results: [] })
  }

  recommendAction(e) {
    if (!this.state.loading) {
      this.setState({ success: false })
      this.setState({ loading: true })
      
      API.makeRecommendation({ payload: {text: this.state.form.fields.text.value} })
      .then(response => {
        console.log(response)
          this.setState({ results: response.data })
          this.setState({ success: true })
          this.setState({ loading: false })
        })
        .catch(response => {
          console.log(response)
          this.setState({ results: [] })
          this.setState({ success: true })
          this.setState({ loading: false })
        })
    }
  }

  render() {

    const form = (
      <form className={this.props.classes.container} noValidate autoComplete="off">
        <FormControl fullWidth className={this.props.classes.margin}>
          <TextField
            id={this.state.form.fields.text.metadata.id}
            name={this.state.form.fields.text.metadata.name}
            multiline={this.state.form.fields.text.metadata.multiline}
            label={this.state.form.fields.text.metadata.label}
            placeholder={this.state.form.fields.text.metadata.placeholder}
            className={this.state.form.fields.text.metadata.className}
            margin={this.state.form.fields.text.metadata.margin}
            rows={this.state.form.fields.text.metadata.rows}
            rowsMax={this.state.form.fields.text.metadata.rowsMax}
            onChange={(event) => this.onChangeFormField(event)}
          />
        </FormControl>
        <FormControl className={this.props.classes.margin}>
          {!this.state.form.fields.text.value && <Tooltip title="You don't have permission to do this">
            <template>
              <Button variant="contained" color="primary" disabled={this.state.loading || true} className={this.props.classes.button} onClick={this.recommendAction}>            
                Send
                <SendIcon></SendIcon>
              </Button>
            </template>
          </Tooltip>}
          {this.state.form.fields.text.value && <div>
            <Button variant="contained" color="primary" disabled={this.state.loading} className={this.props.classes.button} onClick={this.recommendAction}>            
              Send
              {this.state.loading && <CircularProgress size={24} />}
              {!this.state.loading && <SendIcon></SendIcon>}
            </Button>
             
            <Button variant="contained" color="primary" disabled={this.state.loading} className={this.props.classes.button} onClick={this.clearAction}>            
              Clear
              {this.state.loading && <CircularProgress size={24} />}
              {!this.state.loading && <ClearIcon></ClearIcon>}
            </Button>
          </div>}
        </FormControl>
      </form>
    )
    return (
      <Container component="main" maxWidth="lg">
        <Typography variant="h4" component="h5" gutterBottom>
          Recommendation <SearchIcon style={{ fontSize: 28, marginTop: 64 }}></SearchIcon>
        </Typography>
        {form}
        <RecommendationResults items={this.state.results}></RecommendationResults>
      </Container>
    );
  }
  
}

Recommendation.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Recommendation);

