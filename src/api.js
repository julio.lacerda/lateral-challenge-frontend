import axios from 'axios';

class API {
  static URL = 'http://127.0.0.1:8888/'
  static HEADERS = {'Content-Type': 'application/json', withCredentials: true}

  static get({ url }) {
    return axios.get(url, { headers: {...API.HEADERS} })
  }

  static post({ url, payload }) {
    return axios.post(url, payload, { headers: {...API.HEADERS} })
  }
  
  static currentTime(){
    return API.get({ url: API.URL })
  }

  static makeRecommendation({ url, payload }){
    return API.post({ url: `${API.URL}recommendations`, payload: payload })
  }

}

export default API