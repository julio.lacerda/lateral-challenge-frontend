import React from 'react';
import 'typeface-roboto';
import './App.css';
import NavBar from './NavBar';
import Recommendation from './Recommendation';

function App() {
  return (
    <div className="App">
      <NavBar/>
      <Recommendation/>
    </div>
  );
}

export default App;
