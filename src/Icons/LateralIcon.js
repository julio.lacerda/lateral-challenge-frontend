import React from "react";

const SvgLateralIcon = props => (
  <svg width="50pt" height={64} viewBox="0 0 50 48" {...props}>
    <path d="M25.023 27.672l-12.312-6.899L16.094.965" fill="#67aad6" />
    <path d="M37.223 20.824l-12.2 6.848L16.093.965" fill="#e3f6ff" />
    <path d="M25.023 27.672L.848 41.578 12.71 20.773" fill="#adceea" />
    <path d="M37.031 47.055L.848 41.578l24.175-13.906" fill="#82c0f4" />
    <path d="M37.031 47.055L25.023 27.672 49.117 41.66" fill="#d7e9f9" />
    <path d="M25.023 27.672l12.2-6.848L49.117 41.66" fill="#a7ccea" />
  </svg>
);

export default SvgLateralIcon;
