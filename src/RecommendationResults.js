import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import RecommendationItem from './RecommendationItem'
import Container from '@material-ui/core/Container';

class RecommendationResults extends React.Component {

  constructor(props) {
    super(props)
    this.classes = makeStyles(theme => ({
      container: {
        display: 'flex',
        flexWrap: 'wrap',
      },
    }));
  }

  render() {
    const items = this.props.items.map((x) => {
      return <RecommendationItem title={x.title} image={x.image} thumbnail={x.thumbnail} summary={x.summary} url={x.url} key={x.url}/>
    })

    return (
      <Container component="main" maxWidth="lg">
        {items}
      </Container>
    )
  }

}

export default RecommendationResults
